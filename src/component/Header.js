import React from 'react'
class Header extends React.Component {


    render() {
        const isLogin = localStorage.getItem('isLogin')
        return (
            <div>
                <a href="/about">about</a> ||&nbsp;
                <a href="/skill">skill</a>||&nbsp;
                <a href="/gpa">gpa</a>
                {
                    isLogin == 'true' &&
                    <a href='/processLogout'>Logout</a>
                }
            </div>

        )
    }
}
export default Header