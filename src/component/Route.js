import React, {Component} from 'react';
import "bootstrap/dist/css/bootstrap.css"
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import AboutPage from '../cvPage/AboutPage';
import SkillPage from '../cvPage/SkillPage';
import GpaPage from '../cvPage/GpaPage';
import Header from './Header';
import LoginPage from '../cvPage/LoginPage';
import PrivateRoute from './PrivateRoute';

const MainRouting =()=>{
    return(
    <BrowserRouter>       
    {/* บันทึกค่าlogin */}
    <Route path="/processLogin" render={() =>{
        alert("login here")
        localStorage.setItem("isLogin",true);
        return <Redirect to="/gpa"></Redirect>
    }} /> 
     <Route path="/processLogout" render={() =>{
        alert("logout here")
        localStorage.setItem("isLogin",false);
        return <Redirect to="/about"></Redirect>
    }} /> 

        <Route path="/" component={Header} /> 
        <Route path="/about" component={AboutPage} />
        <Route path="/skill" component={SkillPage} />
        <PrivateRoute path="/gpa" component={GpaPage} />
        <Route path="/login" component={LoginPage} />
    </BrowserRouter>
    )}

    export default MainRouting
