import React from 'react';
import { List, Avatar, Icon,Select ,Rate } from 'antd';

const listData = [];
for (let i = 0; i < 23; i++) {
    listData.push({
        href: 'http://ant.design',
        title: `ant design part ${i}`,
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        description:
            'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        content:
            'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
    });
}

const IconText = ({ type, text }) => (
    <span>
        <Icon type={type} style={{ marginRight: 8 }} />
        {text}
    </span>
);
const Listanime = (props) => {


    return (
        <div >

            <List
                itemLayout="vertical"
                loading={props.sendLoading}
                size="large"
                pagination={{
                    onChange: page => {
                        console.log(page);
                    },
                    pageSize: 3,
                }}
                dataSource={props.showAnimesList}

                renderItem={item => (
                    <List.Item
                        key={item.title}
                        actions={[
                            <IconText type="gift" text={item.rated} key="list-vertical-star-o" />,
                            <IconText type="like" text={item.score} key="list-vertical-like-o" />,
                            <IconText type="desktop" text={item.type} key="list-vertical-like-o" />
                        ]}
                        extra={
                            <img
                                width={272}
                                alt="logo"
                                src={item.image_url}
                            />
                        }
                    >
                        <List.Item.Meta
                            title={item.title}
                            description={item.synopsis}
                        />
                        <div>
                            <p>Type:{item.type}</p>
                            <p>score:{item.type}</p>
                            <Rate disabled defaultValue={4.5} />
                        </div>
                    </List.Item>
                )}
            />
        </div>
    );
}

export default Listanime
