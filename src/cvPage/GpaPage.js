import React, { useEffect, useState } from 'react';
import ShowGpa from './ShowGpa';
import { gpaFromServer } from './DataGpa';
import FilterGpa from './FilterGpa';
import "./filterTable.css";

const GpaPage = () => {
  const [gpadata, SetGpa] = useState([])
  const [ShowSemester, setShowSemester] = useState('All')


  const updateSem = (value) => {
    setShowSemester(value)
    // console.log("5555")
  }
  useEffect(() => {
    SetGpa(gpaFromServer);
  }, []);

  return (
    <div class="container">
      <FilterGpa sendChooseSem={gpadata} ShowTableSem={updateSem} />
      {
        ShowSemester == "All" ?
          <div>
            {
              gpadata.map((data) => {
                return <ShowGpa sendSemester={data} />
              })
            }
          </div>
          :
          <ShowGpa sendSemester={gpadata[ShowSemester]} />
      }
    </div>
  )
}
export default GpaPage;
