import React from 'react'
import "./GpaPage.css";

class GpaPage extends React.Component {


    render() {

        return (
            <div>
            <a href="/processLogout">Log out</a>
            <div class="container">
                <div class="card">
                    <div class="RoRTable">
                        <h3><i class="fa fa-pencil-square-o" aria-hidden="true">
                            ปีการศึกษา 2560 ภาคการศึกษาที่ 1</i></h3>
                        <hr />
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr class="table-primary">
                                <th>รหัสกระบวนวิชา</th>
                                <th>ชื่อกระบวนวิชา</th>
                                <th>หน่วยกิต</th>
                                <th>เกรด</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>001101</td>
                            <td>FUNDAMENTAL ENGLISH 1 </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>109100</td>
                            <td>MAN AND ART</td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>851100</td>
                            <td>INTRO TO COMMUNICATION</td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>951100</td>
                            <td>MODERN LIFE AND ANIMATION</td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954100</td>
                            <td>INFO SYSTEM FOR ORG MGMT </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954140</td>
                            <td>IT LITERACY </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                    </table>
                    <div class="TableRS">
                            <div class="col-sm-12 align-center">
                                <table id="GPA-table" class="table table-bordered table-striped" >
                                    <tbody>
                                        <tr>
                                            <td>ผลการศึกษา</td>
                                            <td valign="middle">หน่วยกิตที่ลง</td>
                                            <td valign="middle" >หน่วยกิตที่ได้</td>
                                            <td valign="middle" align="center">เกรดเฉลี่ย</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >ภาคการศึกษานี้</td>
                                            <td valign="middle" class="ng-binding">&nbsp;18&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;18&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >สะสมทั้งหมด</td>
                                            <td valign="middle" class="ng-binding">&nbsp;18&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;18&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <div class="card">
                    <div class="RoRTable">
                        <h3><i class="fa fa-pencil-square-o" aria-hidden="true">
                            ปีการศึกษา 2560 ภาคการศึกษาที่ 2</i></h3>
                        <hr />
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr class="table-primary">
                                <th>รหัสกระบวนวิชา</th>
                                <th>ชื่อกระบวนวิชา</th>
                                <th>หน่วยกิต</th>
                                <th>เกรด</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>001102</td>
                            <td>FUNDAMENTAL ENGLISH 2 </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>013110</td>
                            <td>PSYCHOLOGY AND DAILY LIFE </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>057129</td>
                            <td>T TENNIS FOR LIFE &amp; EXERC </td>
                            <td>1</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>201111</td>
                            <td>THE WORLD OF SCIENCE </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>206171</td>
                            <td>GENERAL MATHEMATICS 1 </td>
                            <td>3</td>
                            <td>W</td>
                        </tr>
                        <tr>
                            <td>954141</td>
                            <td>INFORM AND COMM TECH</td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954142</td>
                            <td>FUNDA COM PROGRAM FOR MM </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>955100</td>
                            <td>LEARNING THROUGH ACTIVITIES 1</td>
                            <td>1</td>
                            <td>A</td>
                        </tr>
                    </table>
                    <div class="TableRS">
                            <div class="col-sm-12 align-center">
                                <table id="GPA-table" class="table table-bordered table-striped" align="center">
                                    <tbody>
                                        <tr>
                                            <td>ผลการศึกษา</td>
                                            <td valign="middle">หน่วยกิตที่ลง</td>
                                            <td valign="middle" >หน่วยกิตที่ได้</td>
                                            <td valign="middle" align="center">เกรดเฉลี่ย</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >ภาคการศึกษานี้</td>
                                            <td valign="middle" class="ng-binding">&nbsp;20&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;17&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >สะสมทั้งหมด</td>
                                            <td valign="middle" class="ng-binding">&nbsp;38&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;35&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>

                <div class="card">
                    <div class="RoRTable">
                        <h3><i class="fa fa-pencil-square-o" aria-hidden="true">
                            ปีการศึกษา 2561 ภาคการศึกษาที่ 1</i></h3>
                        <hr />
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr class="table-primary">
                                <th>รหัสกระบวนวิชา</th>
                                <th>ชื่อกระบวนวิชา</th>
                                <th>หน่วยกิต</th>
                                <th>เกรด</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>001201</td>
                            <td>CRIT READ AND EFFEC WRITE</td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>206171</td>
                            <td>GENERAL MATHEMATICS 1</td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>357101</td>
                            <td>HOUSE INSECT GARDEN PEST</td>
                            <td>1</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954245</td>
                            <td>DATA MANAGEMENT</td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954246</td>
                            <td>ADV COMP PROGRAMMING FOR MM </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954260</td>
                            <td>KNOWLEDGE MANAGEMENT SYSTEM </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954270</td>
                            <td>ELEMENTARY BPM </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                    </table>
                    <div class="TableRS">
                            <div class="col-sm-12 align-center">
                                <table id="GPA-table" class="table table-bordered table-striped" align="center">
                                    <tbody>
                                        <tr>
                                            <td>ผลการศึกษา</td>
                                            <td valign="middle">หน่วยกิตที่ลง</td>
                                            <td valign="middle" >หน่วยกิตที่ได้</td>
                                            <td valign="middle" align="center">เกรดเฉลี่ย</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >ภาคการศึกษานี้</td>
                                            <td valign="middle" class="ng-binding">&nbsp;21&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;18&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >สะสมทั้งหมด</td>
                                            <td valign="middle" class="ng-binding">&nbsp;59&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;53&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>

                <div class="card">
                    <div class="RoRTable">
                        <h3><i class="fa fa-pencil-square-o" aria-hidden="true">
                            ปีการศึกษา 2561 ภาคการศึกษาที่ 2</i></h3>
                        <hr />
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr class="table-primary">
                                <th>รหัสกระบวนวิชา</th>
                                <th>ชื่อกระบวนวิชา</th>
                                <th>หน่วยกิต</th>
                                <th>เกรด</th>
                            </tr>
                        </thead>
                        <tr>
                                    <td>001229 </td>
                                    <td>ENGLISH FOR MEDIA ARTS </td>
                                    <td>3</td>
                                    <td>C+</td>
                                </tr>
                                <tr>
                                    <td>208263</td>
                                    <td>ELEMENTARY STATISTICS </td>
                                    <td>3</td>
                                    <td>C+</td>
                                </tr>
                                <tr>
                                    <td>954240 </td>
                                    <td>WEB PROGRAMMING </td>
                                    <td>1</td>
                                    <td>C+</td>
                                </tr>
                                <tr>
                                    <td>954244</td>
                                    <td>STRUCTURAL ANALYSIS AND DESIGN </td>
                                    <td>3</td>
                                    <td>C+</td>
                                </tr>
                                <tr>
                                    <td>954310</td>
                                    <td>INFO SYS FOR ERP </td>
                                    <td>3</td>
                                    <td>C+</td>
                                </tr>
                                <tr>
                                    <td>954340</td>
                                    <td>ENTERPRISE DATABASE SYSTEM </td>
                                    <td>3</td>
                                    <td>C+</td>
                                </tr>
                                <tr>
                                    <td>955200</td>
                                    <td>LEARNING THROUGH ACTIVITIES 2</td>
                                    <td>1</td>
                                    <td>A</td>
                                </tr>
                    </table>
                    <div class="TableRS">
                            <div class="col-sm-12 align-center">
                                <table id="GPA-table" class="table table-bordered table-striped" align="center">
                                    <tbody>
                                        <tr>
                                            <td>ผลการศึกษา</td>
                                            <td valign="middle">หน่วยกิตที่ลง</td>
                                            <td valign="middle" >หน่วยกิตที่ได้</td>
                                            <td valign="middle" align="center">เกรดเฉลี่ย</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >ภาคการศึกษานี้</td>
                                            <td valign="middle" class="ng-binding">&nbsp;18&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;18&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >สะสมทั้งหมด</td>
                                            <td valign="middle" class="ng-binding">&nbsp;78&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;31&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>

                <div class="card">
                    <div class="RoRTable">
                        <h3><i class="fa fa-book" aria-hidden="true">
                            ปีการศึกษา 2562 ภาคการศึกษาที่ 1</i></h3>
                        <hr />
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr class="table-primary">
                                <th>รหัสกระบวนวิชา</th>
                                <th>ชื่อกระบวนวิชา</th>
                                <th>หน่วยกิต</th>
                                <th>เกรด</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>206171</td>
                            <td>GENERAL MATHEMATICS 1 </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954370</td>
                            <td>ANALYSIS & DESIGN MATS MGMT </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954374</td>
                            <td>SD FOR DIGITAL MARKET </td>
                            <td>1</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954416</td>
                            <td>INFO SYS FOR SCM AND CRM </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954426</td>
                            <td>INTRODUCTION TO E-SERVICE </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954449</td>
                            <td>RAPID APPLICATION DEVELOPMENT </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                        <tr>
                            <td>954477</td>
                            <td>IT FOR PRODUCTION SYS </td>
                            <td>3</td>
                            <td>C+</td>
                        </tr>
                    </table>
                    <div class="TableRS">
                            <div class="col-sm-12 align-center">
                                <table id="GPA-table" class="table table-bordered table-striped" align="center">
                                    <tbody>
                                        <tr>
                                            <td>ผลการศึกษา</td>
                                            <td valign="middle">หน่วยกิตที่ลง</td>
                                            <td valign="middle" >หน่วยกิตที่ได้</td>
                                            <td valign="middle" align="center">เกรดเฉลี่ย</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >ภาคการศึกษานี้</td>
                                            <td valign="middle" class="ng-binding">&nbsp;21&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;21&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" >สะสมทั้งหมด</td>
                                            <td valign="middle" class="ng-binding">&nbsp;99&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;93&nbsp;</td>
                                            <td valign="middle" class="ng-binding">&nbsp;3&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
            </div>

        );
    }
}

export default GpaPage