import React from 'react';

const ShowGpa = (props) => {

    const { sendSemester } = props;
    const { enrolleds, gpa, semester, year } = sendSemester;
    return (
        <table class="table table-hover ">
            <div class="card">
            <div class="fileter-tabss">
                <div class="col-12">
                    <thead>
                        <div class="row termDesign">
                            <tr rowspan="4"><b >{year}{semester}</b></tr>
                        </div>
                        <tr class="colorthead">
                            <th scope="col">No.</th>
                            <th scope="col">Course Id</th>
                            <th scope="col">Course Name</th>
                            <th scope="col">Credit</th>
                            <th scope="col">Grade</th>
                        </tr>
                    </thead>
                    <tbody class="AllCredit">
                        {
                            enrolleds.map((item, index) => {
                                return (
                                    <tr key={index} >
                                        <td >{index + 1}</td>
                                        <td >{item.courseId}</td>
                                        <td >{item.courseName}</td>
                                        <td >{item.credit}</td>
                                        <td >{item.grade}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </div>
            </div>
            <div class="row-12 fileter-tabss">
                <thead >
                    <tr class="colorthead">
                        <th scope="col">Rercord</th>
                        <th scope="col">Current Credits</th>
                        <th scope="col">Additional Credits</th>
                        <th scope="col">GPA</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        <tr >
                            <th>Academic Point</th>
                            <td>{gpa.couseGPA.ca}</td>
                            <td>{gpa.couseGPA.ce}</td>
                            <td>{gpa.couseGPA.gpa}</td>
                        </tr>
                    }
                    {
                        <tr>
                            <th>Total Cumulate Point</th>
                            <td>{gpa.cumulativeGPA.caSem}</td>
                            <td>{gpa.cumulativeGPA.ceSem}</td>
                            <td>{gpa.cumulativeGPA.gpaSem}</td>
                        </tr>
                    }
                </tbody>
            </div>
            </div>
        </table>
    );
}

export default ShowGpa
