import React, { useState } from 'react';
import { Input, Select } from 'antd';

const { Option } = Select;


const { Search } = Input;

const FilterAnime = ({ sendAniType, searchAnime, showListType, sendSelectedAnimeType }) => {

    const [ShowAnimeName, setShowAnimeName] = useState('')
    const [animes, setUsers] = useState([]);

    const FindAnime = (value) => {
        setShowAnimeName(value);
        alert("555")
    }
    const sendAnime = event => searchAnime(event.target.value)


    return (
        <div class="jumbotron Puplebg">
            <div class="row d-flex justify-content-Start">
                <div class="col-5">
                    < h1 class="display-4">Search Anime</h1>
                </div>
                <div class="col-2 selectChose">
                    <Select value={sendSelectedAnimeType} style={{ width: 120 }} onChange={showListType}>
                        <Option value={-1}>All</Option>
                        {
                            sendAniType.map((animes,index) => <Option key={index} value={animes}>{animes}</Option>)
                        }
                    </Select>
                </div>
                <div class="col-4">
                    <form class="form-inline">
                        <Search placeholder="input search text" onSearch={FindAnime} onChange={sendAnime} enterButton />
                    </form>
                </div>
            </div>
        </div>
    )
}
export default FilterAnime;
