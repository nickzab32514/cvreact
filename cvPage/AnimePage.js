import React, { useState, useEffect } from 'react';
import ListAnime from './ListAnime';
import FilterAnime from './FilterAnime';
import "./filterAni.css";


const useAnime = () => {
    const [animes, setAnimes] = useState([]);
    const [animeType, setAnimeType] = useState([]);
    const [ShowAnimeName, setShowAnimeName] = useState('Insert to anime')
    const [isLoading, setIsLoading] = useState(false);
    const [selectedAnimeType, setSelectedAnimeType] = useState(-1);

    const searchNameAninme = (value) => {
        setShowAnimeName(value)
        setIsLoading(true);
        fetchAnime();
    }


    useEffect(() => {
        fetchAnime();
    }, [ShowAnimeName])

    const checkTypeAni = animes => {
        let typeAniList = [];
        animes.map((animes) => {
            if (!typeAniList.includes(animes.type)) {
                typeAniList.push(animes.type)
            }
        })
        setAnimeType(typeAniList)
    }

    const fetchAnime = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=' + ShowAnimeName)
            // console.log(results)
            .then(respose => respose.json())
            .then(data => {
                setTimeout(() => {
                    setIsLoading(false);
                }, 2000)
                setAnimes(data.results)
                checkTypeAni(data.results)
            })
            .catch(error => console.log(error))
    }


    const animeTorender = () => {
        if (selectedAnimeType == -1) {
            return animes;
        } else {
            // return animes.filter((anime) => {
            //     return anime.type == selectedAnimeType;
            // })
            return animes.filter(anime => anime.type == selectedAnimeType)
        }
    }

    return (
        <div class="container">
            <FilterAnime searchAnime={searchNameAninme}
                sendAniType={animeType}
                sendSelectedAnimeType={selectedAnimeType}
                showListType={setSelectedAnimeType} />
            <ListAnime showAnimesList={animeTorender()} sendLoading={isLoading} />
        </div>
    )
}

export default useAnime
