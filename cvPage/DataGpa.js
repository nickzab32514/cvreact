export const gpaFromServer = [
    {
        year:2560,
        semester: " Semester 1",
        enrolleds: [
            {
                courseId: '001101',
                courseName: 'FUNDAMENTAL ENGLISH 1',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '109100',
                courseName: 'MAN AND ART',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '851100',
                courseName: 'INTRO TO COMMUNICATION',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '951100',
                courseName: 'MODERN LIFE AND ANIMATION',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954100',
                courseName: 'INFO SYSTEM FOR ORG MGMT',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954140',
                courseName: 'IT LITERACY ',
                credit: 3,
                grade: 'C+'
            }
        ],
        gpa: {
            couseGPA: {
                ca: 18,
                ce: 18,
                gpa: 4
            },
            cumulativeGPA: {
                caSem: 18,
                ceSem: 18,
                gpaSem: 4
            }
        }
    },
    {
        year:2560,
        semester: " Semester 2",
        enrolleds: [
            {
                courseId: '001102',
                courseName: 'FUNDAMENTAL ENGLISH 2',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '013110',
                courseName: 'PSYCHOLOGY AND DAILY LIFE',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '057129',
                courseName: 'T TENNIS FOR LIFE EXERC',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '201111',
                courseName: 'THE WORLD OF SCIENCE ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '206171',
                courseName: 'GENERAL MATHEMATICS 1',
                credit: 3,
                grade: 'W'
            },
            {
                courseId: '954142',
                courseName: 'FUNDA COM PROGRAM FOR MM',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '955100',
                courseName: 'LEARNING THROUGH ACTIVITIES 1 ',
                credit: 3,
                grade: 'A'
            }
        ],
        gpa: {
            couseGPA: {
                ca: 20,
                ce: 17,
                gpa: 3
            },
            cumulativeGPA: {
                caSem: 37,
                ceSem: 35,
                gpaSem: 3
            }
        }
    },
    {   year:2561,
        semester: " Semester 1",
        enrolleds: [
            {
                courseId: '001201',
                courseName: 'CRIT READ AND EFFEC WRITE',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '206171',
                courseName: 'GENERAL MATHEMATICS 1',
                credit: 3,
                grade: 'W'
            },
            {
                courseId: '357101',
                courseName: 'HOUSE INSECT GARDEN PEST',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954245',
                courseName: 'DATA MANAGEMENT',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954246',
                courseName: 'ADV COMP PROGRAMMING FOR MM ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954260',
                courseName: 'KNOWLEDGE MANAGEMENT SYSTEM ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954270',
                courseName: 'ELEMENTARY BPM ',
                credit: 3,
                grade: 'C+'
            }
        ],
        gpa: {
            couseGPA: {
                ca: 21,
                ce: 18,
                gpa: 3
            },
            cumulativeGPA: {
                caSem: 59,
                ceSem: 53,
                gpaSem: 3
            }
        }
    },
    {
        year:2561,
        semester: " Semester 2",
        enrolleds: [
            {
                courseId: '001229',
                courseName: 'ENGLISH FOR MEDIA ARTS',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '208263',
                courseName: 'ELEMENTARY STATISTICS ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954240',
                courseName: 'WEB PROGRAMMING ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954244',
                courseName: 'STRUCTURAL ANALYSIS AND DESIGN ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954310',
                courseName: 'INFO SYS FOR ERP ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954340',
                courseName: 'ENTERPRISE DATABASE SYSTEM',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '955200',
                courseName: 'LEARNING THROUGH ACTIVITIES 2 ',
                credit: 1,
                grade: 'A'
            }
        ],
        gpa: {
            couseGPA: {
                ca: 18,
                ce: 18,
                gpa: 3
            },
            cumulativeGPA: {
                caSem: 78,
                ceSem: 31,
                gpaSem: 3
            }
        }
    },
    {
        year:2562,
        semester: " Semester 1",
        enrolleds: [
            {
                courseId: '206171',
                courseName: 'GENERAL MATHEMATICS 1 ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954370',
                courseName: 'ANALYSIS & DESIGN MATS MGMT',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954374',
                courseName: 'SD FOR DIGITAL MARKET ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954416',
                courseName: 'INFO SYS FOR SCM AND CRM ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954426',
                courseName: 'INTRODUCTION TO E-SERVICE  ',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954449',
                courseName: 'RAPID APPLICATION DEVELOPMENT',
                credit: 3,
                grade: 'C+'
            },
            {
                courseId: '954477',
                courseName: 'IT FOR PRODUCTION SYS  ',
                credit: 3,
                grade: 'C+'
            }
        ],
        gpa: {
            couseGPA: {
                ca: 21,
                ce: 21,
                gpa: 3
            },
            cumulativeGPA: {
                caSem: 99,
                ceSem: 93,
                gpaSem: 3
            }
        }
    }
]