import React from 'react'

class LoginPage extends React.Component {


    render() {

        return (
            <div class="contanier">
                <h1>login page</h1>
                <form>
                    <div class="row">
                        <label class="col-sm-1" for="exampleInputEmail1">Email address</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-1" for="exampleInputPassword1">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="exampleInputPassword1" />
                        </div>
                    </div>
                    <input type="button" value="Login" onClick={
                        ()=>{ 
                            //Redirect
                            window.location="/processLogin"
                        }
                     }
                    />
                </form>
            </div>
        );
    }
}

export default LoginPage