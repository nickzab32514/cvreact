import React,{useState} from 'react';


const FilterGpa = (props) => {
    const [ShowSemester,setShowSemester] = useState('')

    const UpdateSemester=(event)=>{
        setShowSemester(event.target.value);
        props.ShowTableSem(event.target.value);
    }

    return (
        <nav class="navbar navbar-expand-lg navbar-light Barnav">
             <i class="whiteFF" >Academic Report Of University</i>
            <form class="form-inline">
                <div class="form-group fileter-tab formLefttt">
                    <div class="row">
                            <label class="whiteFFsmall BByellow">Semester</label>
                        <div class="col-3">
                            <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" onChange={UpdateSemester}>
                                <option  selected>All</option>
                                {
                                props.sendChooseSem.map((sem,index)=>{
                                    return(
                                    <option value={index}>{sem.year}{sem.semester}</option>
                                    )
                                })
                                }
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </nav>
    )
}
export default FilterGpa;
